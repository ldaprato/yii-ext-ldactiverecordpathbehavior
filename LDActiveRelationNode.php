<?php

class LDActiveRelationNode
{

	/**
	 * @var CActiveRelation the relation represented by this tree node
	 */
	private $_relation;

	/**
	 * @var CActiveRelation the master relation
	 */
	private $_master;

	/**
	 * @var CActiveRelation the slave relation
	 */
	private $_slave;

	/**
	 * @var CActiveRecord the model associated with this tree node
	 */
	private $_model;

	/**
	 * @var array list of child relation nodes
	 */
	private $_children = array();

	/**
	 * @var LDActiveRelationNode this node's parent
	 */
	private $_parent;

	/**
	 * Constructor.
	 * 
	 * @param mixed $relation
	 *        	the relation (if the third parameter is not null)
	 *        	or the model (if the third parameter is null) associated with this tree node.
	 * @param LDActiveRelationNode $parent
	 *        	the parent tree node
	 * @param integer $_id
	 *        	the ID of this tree node that is unique among all the tree nodes
	 */
	public function __construct($relation, $parent = null, $model = null)
	{
		if($parent instanceof LDActiveRelationNode)
		{
			$this->_relation = $relation;
			$this->_parent = $parent;
			$this->_model = $model === null ? LDActiveRecordPathNode::ensureCActiveRecord($relation->className) : $model;
		}
		else // root element, the first parameter is the model and the second is the relation of this node.
		{
			$this->_model = $relation;
			$this->_relation = $parent;
			$this->buildRelationTree($parent->name);
		}
	}
	
	private function buildRelationTree($with, $options = null)
	{
		$parent = $this;
		if($parent->_relation instanceof CStatRelation)
		{
			throw new CDbException(Yii::t('yii', 'The STAT relation "{name}" cannot have child relations.', array(
				'{name}' => $parent->_relation->name
			)));
		}
		
		if(is_string($with))
		{
			if(($pos = strrpos($with, '.')) !== false)
			{
				$parent = $parent->buildRelationTree(substr($with, 0, $pos));
				$with = substr($with, $pos + 1);
			}
				
			// named scope
			$scopes = array();
			if(($pos = strpos($with, ':')) !== false)
			{
				$scopes = explode(':', substr($with, $pos + 1));
				$with = substr($with, 0, $pos);
			}

			if(isset($parent->_children[$with]) && $parent->_children[$with]->_master === null)
			{
				return $parent->_children[$with];
			}
				
			if(($relation = $parent->getModel()->getActiveRelation($with)) === null)
			{
				throw new CDbException(Yii::t('yii', 'Relation "{name}" is not defined in active record class "{class}".', array(
					'{class}' => $parent->getModelName(),
					'{name}' => $with
				)));
			}
				
			$relation = clone $relation;
			$model = LDActiveRecordPathNode::ensureCActiveRecord($relation->className, $parent->getModel()->getScenario());
				
			if($relation instanceof CActiveRelation)
			{
				$oldAlias = $model->getTableAlias(false, false);
				if(isset($options['alias']))
				{
					$model->setTableAlias($options['alias']);
				}
				elseif($relation->alias === null)
				{
					$model->setTableAlias($relation->name);
				}
				else
				{
					$model->setTableAlias($relation->alias);
				}
			}
				
			if(!empty($relation->scopes))
			{
				$scopes = array_merge($scopes, (array)$relation->scopes); // no need for complex merging
			}
				
			if(!empty($options['scopes']))
			{
				$scopes = array_merge($scopes, (array)$options['scopes']); // no need for complex merging
			}
				
			$model->resetScope(false);
			$criteria = $model->getDbCriteria();
			$criteria->scopes = $scopes;
			$model->beforeFindInternal();
			$model->applyScopes($criteria);
				
			// select has a special meaning in stat relation, so we need to ignore select from scope or model criteria
			if($relation instanceof CStatRelation)
			{
				$criteria->select = '*';
			}
				
			$relation->mergeWith($criteria, true);
				
			// dynamic options
			if($options !== null)
			{
				$relation->mergeWith($options);
			}
				
			if($relation instanceof CActiveRelation)
			{
				$model->setTableAlias($oldAlias);
			}
				
			if($relation instanceof CStatRelation)
			{
				return new LDActiveRelationNode($relation, $parent, $model);
			}
			
			if(isset($parent->_children[$with]))
			{
				$node = $parent->_children[$with];
				$node->_relation = $relation;
			}
			else
			{
				$node = new LDActiveRelationNode($relation, $parent, $model);
			}

			if(!empty($relation->through))
			{
				$slave = $parent->buildRelationTree($relation->through, array('select' => ''));
				$slave->_master = $node;
				$node->_slave = $slave;
			}
			
			$parent->_children[$with] = $node;

			if(!empty($relation->with))
			{
				$node->buildRelationTree($relation->with);
			}
			return $node;
		}
	
		// $with is an array, keys are relation name, values are relation spec
		foreach($with as $key => $value)
		{
			if(is_string($value)) // the value is a relation name
			{
				$parent->buildRelationTree($value);
			}
			else if(is_string($key) && is_array($value))
			{
				$parent->buildRelationTree($key, $value);
			}
		}
	}
	
	/**
	 * @return CActiveRelation the relation represented by this tree node
	 */
	public function getRelation()
	{
		return $this->_relation;
	}

	/**
	 * @return CActiveRelation the master relation
	 */
	public function getMaster()
	{
		return $this->_master;
	}
	
	/**
	 * @return CActiveRelation the slave relation
	 */
	public function getSlave()
	{
		return $this->_slave;
	}
	
	/**
	 * @return CActiveRecord the model associated with this tree node
	 */
	public function getModel()
	{
		return $this->_model;
	}
	
	/**
	 * @return array list of child relation nodes
	 */
	public function getChildren()
	{
		return $this->_children;
	}
	
	/**
	 * @return LDActiveRelationNode this node's parent
	 */
	public function getParent()
	{
		return $this->_parent;
	}
	
	/**
	 * @param mixed $model
	 * @param mixed $relation
	 * @return LDActiveRelationNode
	 */
	public function searchSlaves($model, $relation)
	{
		$slave = $this->getSlave();
		if($slave !== null && !$slave->equals($model, $relation))
		{
			$slave = $slave->searchSlaves($model, $relation);
		}
		return $slave;
	}
	
	/**
	 * @param mixed $model
	 * @param mixed $relation
	 * @return LDActiveRelationNode
	 */
	public function searchChildren($model, $relation)
	{
		foreach($this->getChildren() as $child)
		{
			if($child->equals($model, $relation))
			{
				return $child;
			}
			$child = $child->searchChildren($model, $relation);
			if($child !== null)
			{
				return $child;
			}
		}
		return null;
	}
	
	/**
	 * @param mixed $model
	 * @param mixed $relation
	 * @return boolean
	 */
	public function hasChild($model, $relation)
	{
		return $this->searchChildren($model, $relation) !== null;
	}
	
	/**
	 * @param mixed $model
	 * @param mixed $relation
	 * @return LDActiveRelationNode
	 */
	public function search($model, $relation)
	{
		if($this->equals($model, $relation))
		{
			return $this;
		}
		$node = $this->searchSlaves($model, $relation);
		if($node === null)
		{
			return $this->searchChildren($model, $relation);
		}
		return $node;
	}
	
	/**
	 * @param mixed $model CActiveRecord or AR class name
	 * @param mixed $relation CActiveRelation or relation name
	 * @return boolean True if this node has the specified relation
	 */
	public function contains($model, $relation)
	{
		return $this->search($model, $relation) !== null;
	}
	
	/**
	 * @param mixed $model
	 * @param mixed $relation
	 * @return boolean
	 */
	public function equals($model, $relation)
	{
		return strcasecmp($this->getModelName(), ($model instanceof CActiveRecord ? get_class($model) : (string)$model)) === 0 && ($relation instanceof CActiveRelation ? $relation->name : (string)$relation) === $this->getRelation()->name;
	}
	
	/**
	 * @return string the class name of the model associated with this tree node
	 */
	public function getModelName()
	{
		return get_class($this->getModel());
	}
	
	/**
	 * @return string the name of the relation this node represents
	 */
	public function getRelationName()
	{
		return $this->getRelation()->name;
	}
	
	/**
	 * @return string A fully qualified relation name for containing the name of the model that own this node's relation and this node's relation name
	 */
	public function getName()
	{
		return self::name($this->getParent() === null ? $this->getModelName() : $this->getParent()->getModelName(), $this->getRelationName());
	}
	
	/**
	 * @return string Returns the name of this relation node
	 */
	public function __toString()
	{
		return $this->getName();
	}
	
	/**
	 * @var string The string used to glue fully qualified relation name parts together.
	 */
	const SEGMENT_GLUE = '-';
	
	/**
	 * Creates a fully qualified relation name from a CActiveRecord instance or class string name and a CActiveRelation instance or relation string name
	 *
	 * @param CActiveRecord|string $model The AR instance or string name of the AR
	 * @param CActiveRelation|string $relationName The realtion name
	 * @return string The fully qualified relation name
	 */
	public static function name($model, $relation)
	{
		return ($model instanceof CActiveRecord ? get_class($model) : (string)$model).self::SEGMENT_GLUE.($relation instanceof CBaseActiveRelation ? $relation->name : (string)$relation);
	}
	
	/**
	 * Extracts the model name from a fully qualified relation name.
	 * False is returned if the name is not a fully qualified relation name.
	 *
	 * @param string $segmentName
	 * @return boolean|string The model name or false if not passed a fully qualified relation name
	 */
	public static function extractModelName($segmentName)
	{
		return strlen($segmentName) < 3 || ($i = strpos($segmentName, self::SEGMENT_GLUE, 1)) === false ? false : substr($segmentName, 0, $i);
	}
	
	/**
	 * Extracts the relation name from a fully qualified relation name.
	 * False is returned if the name is not a fully qualified relation name.
	 *
	 * @param string $segmentName
	 * @return boolean|string The relation name or false if not passed a fully qualified relation name
	 */
	public static function extractRelationName($segmentName)
	{
		return strlen($segmentName) < 3 || ($i = strpos($segmentName, self::SEGMENT_GLUE, 1)) === false ? false : substr($segmentName, $i + 1);
	}

}
