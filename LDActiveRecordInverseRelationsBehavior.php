<?php

/**
 * 
 * @author Louis A. DaPrato <l.daprato@gmail.com>
 *
 */
class LDActiveRecordInverseRelationsBehavior extends CActiveRecordBehavior
{

	private $_inverseRelations;
	
	/**
	 * Returns a list of this model's relation names as keys and their respective inverse CActiveRelation from the related model as values.
	 * Relations are paired based on the order in which they are defined in this model and the related model.
	 * Only instances of CActiveRelation will be paired (Avoids STAT relations).
	 * 
	 * @param boolean $refresh Whether to refresh the pairing of inverse relations
	 * @return array list of inverse relations in the form array('A relation name of this model' => inverse CActiveRelation instance from related model, ...)
	 */
	public function getInverseRelations($refresh = false)
	{
		if($refresh || !isset($this->_inverseRelations))
		{
			$model = $this->getOwner();
			$this->_inverseRelations = array();
			// Group this model's relations by their className
			$relatedModels = array();
			foreach(array_keys($model->relations()) as $relationName)
			{
				$relation = $model->getActiveRelation($relationName);
				$relatedModels[$relation->className][$relationName] = $relation;
			}
			// Loop over the grouped relations
			foreach($relatedModels as $relatedClassName => $relations)
			{
				// While there are still relations to examine for the current class name
				while(!empty($relations))
				{
					// Grab the next relation's name and configuration and remove it from the list of relations for the current model
					$relation = reset($relations);
					$relationName = key($relations);
					unset($relations[$relationName]);
					// If the relation is a CActiveRelation then loop over the relations of the related model.
					if($relation instanceof CActiveRelation)
					{
						$relatedModel = call_user_func(array($relation->className, 'model'));
						foreach(array_keys($relatedModel->relations()) as $relatedRelationName)
						{
							$relatedRelation = $relatedModel->getActiveRelation($relatedRelationName);
							// If the relation is a CActiveRelation (not a STAT relation) and it relates back to this class name
							if($relatedRelation instanceof CActiveRelation && strcasecmp($relatedRelation->className, get_class($this->getOwner())) === 0)
							{
								$this->_inverseRelations[$relationName] = $relatedRelation;
								// If all relations have been paired for this model break
								if(empty($relations))
								{
									break;
								}
								// Get the next relation while this model has more relations and the current relation is not a CActiveRelation
								do
								{
									$relation = reset($relations);
									$relationName = key($relations);
									unset($relations[$relationName]);
								}
								while(!$relation instanceof CActiveRelation && !empty($relations));
								// If we exited the loop with a relation that was not an instance of CActiveRelation then we are done pairing relations for the current related model
								if(!$relation instanceof CActiveRelation)
								{
									break;
								}
							}
						}
					}
				}
			}
		}
		return $this->_inverseRelations;
	}
	
	public function getInverseRelation($relation, $refresh = false)
	{
		$inverseRelations = $this->getInverseRelations($refresh);
		return $inverseRelations[$relation];
	}
	
}
