<?php

/**
 *
 * @author Louis A. DaPrato <l.daprato@gmail.com>
 *        
 */
class LDActiveRecordPathNode
{

	/**
	 *
	 * @var CActiveRecord an active record in the path this node is a part of
	 */
	private $_model;

	/**
	 *
	 * @var LDActiveRelationNode The relation tree node of the relation flowing (pointing) towards the source (the beginning of the path this node is a part of)
	 */
	private $_source;

	/**
	 *
	 * @var LDActiveRelationNode The relation tree node of the relation flowing (pointing) towards the sink (the end of the path this node is a part of)
	 */
	private $_sink;

	/**
	 *
	 * @var array list of attribute names that have been set for the model of this node through this node
	 */
	private $_attributeNames;

	/**
	 * Constructs a new AR path node
	 *
	 * @param mixed $source        	
	 * @param mixed $sink        	
	 * @throws CException
	 */
	public function __construct($source, $sink = null, $values = array())
	{
		if($sink === null) // Creating the first node of a path
		{
			$this->_model = self::ensureCActiveRecord($source);
		}
		else if($source instanceof LDActiveRecordPathNode) // Building path forward
		{
			$this->initRelations($source, $sink, false);
		}
		else if($sink instanceof LDActiveRecordPathNode) // Building path backward
		{
			$this->initRelations($sink, $source, true);
		}
		else
		{
			throw new CException('Invalid parameters.');
		}
		
		$this->setAttributes($values);
	}

	/**
	 * Initializes the relations of this node and the node that this node is being attached to (the parent node)
	 *
	 * @param LDActiveRecordPathNode $parentNode
	 *        	the node that this node is being attached to
	 * @param mixed $relationFromParent
	 *        	The relation that connects the parent node to this node
	 * @param boolean $reverseFlow
	 *        	Flag to indicate direction of flow. If true then the parent is the sink and this is the source. If false the parent is the source and this is the sink.
	 * @throws CException throws exception if a relation or AR are cannot be found or are not configured correctly for a path node.
	 */
	protected function initRelations($parentNode, $relationFromParent, $reverseFlow)
	{
		$relationFromParent = self::ensureCActiveRelation($parentNode->getModel(), $relationFromParent);
		$relationToParent = $parentNode->getModel()->getInverseRelation($relationFromParent->name);
		$this->_model = self::ensureCActiveRecord($relationFromParent->className, $parentNode->getModel()->getScenario());
		
		if($reverseFlow)
		{
			$parentNode->_source = new LDActiveRelationNode($parentNode->getModel(), $relationFromParent);
			$this->_sink = new LDActiveRelationNode($this->getModel(), $relationToParent);
		}
		else
		{
			$parentNode->_sink = new LDActiveRelationNode($parentNode->getModel(), $relationFromParent);
			$this->_source = new LDActiveRelationNode($this->getModel(), $relationToParent);
		}
	}
	
	/**
	 * Convert this node to an array
	 *
	 * @param boolean $reverseFlow
	 * @return array
	 */
	public function toArray($reverseFlow = false)
	{
		$relation = $reverseFlow ? $this->getSource() : $this->getSink();
		if($this->hasAttributes())
		{
			return $relation === null ? array($this->getModelName() => $this->getAttributes()) : array($relation->getRelationName() => $this->getAttributes());
		}
		return $relation === null ? array() : $relation->getRelation()->name;
	}

	/**
	 *
	 * @return CActiveRecord
	 */
	public function getModel()
	{
		return $this->_model;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasAttributes()
	{
		return !empty($this->_attributeNames);
	}

	/**
	 *
	 * @return array list of attribute names that have been set for the model of this node through this node
	 */
	public function getAttributeNames()
	{
		return $this->_attributeNames;
	}

	/**
	 *
	 * @param array $values        	
	 */
	public function setAttributes($values)
	{
		$this->getModel()->setAttributes($values);
		$this->_attributeNames = array_intersect($this->getModel()->getSafeAttributeNames(), array_keys($values));
	}

	/**
	 *
	 * @return array attribute values indexed by attribute names.
	 */
	public function getAttributes()
	{
		return $this->getModel()->getAttributes($this->getAttributeNames());
	}

	/**
	 *
	 * @return string
	 */
	public function getModelName()
	{
		return get_class($this->getModel());
	}

	/**
	 *
	 * @return LDActiveRelationNode
	 */
	public function getSource()
	{
		return $this->_source;
	}

	/**
	 *
	 * @return LDActiveRelationNode
	 */
	public function getSink()
	{
		return $this->_sink;
	}

	/**
	 * Unlinks this node's source relation (sets it to null)
	 */
	public function unlinkSource()
	{
		$this->_source = null;
	}

	/**
	 * Unlinks this node's sink relation (sets it to null)
	 */
	public function unlinkSink()
	{
		$this->_sink = null;
	}

	/**
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->getModelName();
	}

	/**
	 *
	 * @param mixed $value        	
	 * @param string $scenario        	
	 * @throws CException
	 * @return CActiveRecord
	 */
	public static function ensureCActiveRecord($value, $scenario = null)
	{
		if($value instanceof CActiveRecord) // The model is already a CActiveRecord instance!
		{
			$m = $value;
		}
		else // If the model is not already a CActiveRecord instance try to massage it into one.
		{
			// If it is not an array assume it is a string class name (casting if not)
			$value = (string)$value;
			if($scenario === null) // If no scenario is specified
			{
				$m = new $value();
			}
			else // If a scenario is specified construct it with the scenario
			{
				$m = new $value($scenario);
			}
			
			if(!$m instanceof CActiveRecord) // Make sure we found an instance of a CActiveRecord throwing an exception if we didn't
			{
				throw new CException('Expected instance of CActiveRecord. Value "' . $value . '" is not an instance of CActiveRecord and could not be converted to one either.');
			}
		}
		
		if($scenario !== null) // Make sure the scenario on the model is set if one is specified
		{
			$m->setScenario($scenario);
		}
		return $m;
	}

	/**
	 *
	 * @param CActiveRecord $model        	
	 * @param mixed $value        	
	 * @throws CException
	 * @return CActiveRelation
	 */
	public static function ensureCActiveRelation($model, $value)
	{
		if(!$model instanceof CActiveRecord)
		{
			throw new CException('Invalid argument. The argument for parameter "$model" must be an instance of a CActiveRecord.');
		}
		
		if($value instanceof CActiveRelation) // The model is already a CActiveRecord instance!
		{
			$value = $value->name;
			$r = $model->getActiveRelation($value);
		}
		else if($value instanceof CBaseActiveRelation) // Make sure it is not a base relation. Cannot use stat relations.
		{
			throw new CException('Relation "' . $value->name . '" must be an instance of CActiveRelation.');
		}
		else
		{
			$value = (string)$value;
		}
		
		$r = $model->getActiveRelation($value);
		if($r === null)
		{
			throw new CException('The relation "' . $value . '" is not defined for the model class "' . get_class($model) . '"');
		}
		return $r;
	}

}
