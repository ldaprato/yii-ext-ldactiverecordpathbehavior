<?php

Yii::import('ext.LDActiveRecordPathBehavior.*');

/**
 * An instance of this class represents a path of relations through a database with parameters for each segment of the path.
 * 
 * @author Louis A. DaPrato <l.daprato@gmail.com>
 *
 */
class LDActiveRecordPathBehavior extends CActiveRecordBehavior
{
	
	public $pathVarName = 'path';
	
	/**
	 * @var LDActiveRecordPath The current path if set.
	 */
	private $_path;
	
	/**
	 * Sets the relation path and model. Path will be verified..
	 * 
	 * @param $path mixed array the breadcrumbs leading from this model to the model at the start of the path to set. LDActiveRecordPath the explicit path to set.
	 * @throws CException throws exception if the $path parameter is not an array of breadcrumbs or an instance of LDActiveRecordPath or it is an incompatible instance of LDActiveRecordPath (models do not match)
	 */
	public function setRelationPath($path, $attributes = array())
	{
		$owner = $this->getOwner();
		if(is_array($path))
		{
			$this->_path = new LDActiveRecordPath($owner, $attributes, $path, true);
		}
		else if($path instanceof LDActiveRecordPath)
		{
			$this->_path = $path;
		}
		else 
		{
			throw new CException('Invalid parameter "$path". Path must be an array of breadcrumbs or an instance of LDActiveRecordPath');
		}
		$owner->getDbCriteria()->scopes['path'] = 'path';
	}
	
	/**
	 * Returns the relation path.
	 * 
	 * @return LDActiveRecordPath The relation path
	 */
	public function getRelationPath()
	{
		return $this->_path;
	}
	
	/**
	 * Scopes the owner of this CActiveRecordBehavior by the current path.
	 * 
	 * @throws CException Throws exception if a relation cycle is detected
	 * @return CActiveRecord The owner of this CActiveRecordBehavior
	 */	
	public function path()
	{
		$owner = $model = $this->getOwner();
		$criteria = array();
		$relationPath = $this->getRelationPath();
		if(!$relationPath->getIsEmpty())
		{
			$criteria['group'] = $model->generateGroupBy();
		}
		$with = &$criteria;
		for($i = count($relationPath) - 1; $i > 0; $i--)
		{
			$model = $relationPath[$i - 1]->getModel();
			$relation = $relationPath[$i]->getSource()->getRelation();
	
			// Make sure with is an empty array
			if(!isset($with['with']))
			{
				$with['with'] = array();
			}
			
			$with['with'][$relation->name] = $model->buildSearchCriteria($relationPath[$i - 1]->getAttributes(), array('select' => false, 'together' => true, 'with' => isset($relation->with) ? $relation->with : array()), $relation->name)->toArray();
			$with['with'][$relation->name]['joinType'] = 'INNER JOIN';
	
			$with = &$with['with'][$relation->name];
		}

		$owner->getDbCriteria()->mergeWith($criteria);
		return $owner;
	}

	/**
	 * This is supposed to return a list of path names that dependent on this model
	 * This is useful for knowing what grids or views to update if this model is modified
	 * @return array
	 */
	public function getRelatedPathNames()
	{
		return array();
	}
	
}
