<?php

/**
 * 
 * @author Louis A. DaPrato <l.daprato@gmail.com>
 *
 */
class LDActiveRecordPath implements ArrayAccess, IteratorAggregate, Countable
{
	
	/**
	 * @var string The string used to glue relation node name parts together.
	 */
	const NAME_GLUE = '--';

	/**
	 *
	 * @var array A list of path nodes
	 */
	private $_path;

	/**
	 * Constructs a new AR relation path
	 *
	 * @param mixed $model
	 *        	CActiveRecord or string class name of a CActiveRecord to begin constructing the path with.
	 * @param mixed $path
	 *        	The relations to become the AR path. This parameter may be many things.
	 *        	A string name of a single relation.
	 *        	A single CActiveRelation.
	 *        	An array of relation names.
	 *        	An array of CActiveRelations.
	 *        	An array of arrays where the keys of the arrays are the relation names and the values are the condition parameters.
	 * @param boolean $reverseFlow
	 *        	The direction of flow that the path of relations have.
	 *        	If true the relations should start at the specified model (sink) and lead to the source.
	 *        	If false the relations should start at the specified model (source) and lead to the sink.
	 *        	Defaults to false.
	 */
	public function __construct($model, $attributes = array(), $path = array(), $reverseFlow = false)
	{
		$this->clear($model, $attributes);
		if($reverseFlow)
		{
			$this->unshift($path);
		}
		else
		{
			$this->push($path);
		}
	}

	/**
	 * Searches this path for the specified model and relation
	 *
	 * @param mixed $model CActiveRecord or string CActiceRecord className
	 * @param mixed $relation CActiveRelation of string relation name
	 * @return boolean True if a node containing the specified model and relation exists. False otherwise.
	 */
	public function contains($model, $relation)
	{
		return $this->search($model, $relation) !== null;
	}

	/**
	 * Searches this path for the specified model and relation
	 * 
	 * @param mixed $model CActiveRecord or string CActiceRecord className
	 * @param mixed $relation CActiveRelation of string relation name
	 * @return LDActiveRecordPathNode The node in this path containing the specified model and relation or null if the node could not be found.
	 */
	public function search($model, $relation)
	{
		foreach($this as $node)
		{
			if($node->getSink() !== null)
			{
				$node = $node->getSink()->search($model, $relation);
				if($node !== null)
				{
					return $node;
				}
			}
		}
		return null;
	}

	/**
	 * Convert this path to an array of relation names and parameters
	 *
	 * @param boolean $reverseFlow        	
	 * @return array
	 */
	public function toArray($reverseFlow = false)
	{
		$path = array();
		if($reverseFlow)
		{
			for($i = count($this) - 1; $i > 0; $i--)
			{
				$path[] = $this[$i - 1]->hasAttributes() ? array($this[$i]->getSource()->getRelationName() => $this[$i - 1]->getAttributes()) : $this[$i]->getSource()->getRelationName();
			}
		}
		else
		{
			for($i = 0; $i < count($this) - 1; $i++)
			{
				$path[] = $this[$i + 1]->hasAttributes() ? array($this[$i]->getSink()->getRelationName() => $this[$i + 1]->getAttributes()) : $this[$i]->getSink()->getRelationName();
			}
		}
		return $path;
	}
	
	/**
	 * Convert this path to an array of relation names
	 *
	 * @param boolean $reverseFlow
	 * @return array
	 */
	public function getRelationNames($reverseFlow = false)
	{
		$path = array();
		if($reverseFlow)
		{
			for($i = count($this) - 1; $i > 0; $i--)
			{
				$path[] = $this[$i]->getSource()->getRelationName();
			}
		}
		else
		{
			for($i = 0; $i < count($this) - 1; $i++)
			{
				$path[] = $this[$i]->getSink()->getRelationName();
			}
		}
		return $path;
	}
	
	/**
	 * Convert this path to an array of relation node names
	 *
	 * @param boolean $reverseFlow
	 * @return array
	 */
	public function getRelationNodeNames($reverseFlow = false)
	{
		$path = array();
		if($reverseFlow)
		{
			for($i = count($this) - 1; $i > 0; $i--)
			{
				$path[] = $this[$i]->getSource()->getName();
			}
		}
		else
		{
			for($i = 0; $i < count($this) - 1; $i++)
			{
				$path[] = $this[$i]->getSink()->getName();
			}
		}
		return $path;
	}
	
	/**
	 * Returns a list of relations for the last model in this path that have not yet been explored (the relation is not part of this path or this path's relation tree).
	 * 
	 * @return array List of unexplored relations
	 */
	public function getUnexploredSegments()
	{
		$model = $this->getLast()->getModel();
		$unexplored = $model->getInverseRelations();
		foreach($unexplored as $relationName => $inverseRelation)
		{
			if($this->contains($model->getActiveRelation($relationName)->className, $inverseRelation))
			{
				unset($unexplored[$relationName]);
			}
		}
		return $unexplored;
	}
	
	/**
	 * Whether this path is empty.
	 * The path is considered empty if only the root node is present
	 * 
	 * @return boolean True if empty false otherwise
	 */
	public function getIsEmpty()
	{
		return count($this) === 1;
	}

	/**
	 * Returns an iterator for traversing the nodes in this path
	 * This method is required by the interface IteratorAggregate.
	 * 
	 * @return Iterator an iterator for traversing the items in the stack.
	 */
	public function getIterator()
	{
		return new LDActiveRecordPathIterator($this);
	}

	/**
	 * Removes all nodes from the path and sets the root node to contain the specified CActiveRecord.
	 * 
	 * @param mixed $model
	 *        	CActiveRecord or string class name of a CActiveRecord to become the new root of this path.
	 */
	public function clear($model, $attributes = array())
	{
		$this->_path = array(
			new LDActiveRecordPathNode($model, null, $attributes)
		);
	}

	/**
	 * Returns the first node in the path.
	 * Unlike {@link shift()}, this method does not remove the node from the path.
	 * 
	 * @return LDActiveRecordPathNode the first node in the path
	 */
	public function getFirst()
	{
		return $this->_path[0];
	}

	/**
	 * Returns the last node in the path.
	 * Unlike {@link pop()}, this method does not remove the node from the path.
	 * 
	 * @return LDActiveRecordPathNode the last node in the path
	 */
	public function getLast()
	{
		return $this->_path[count($this->_path) - 1];
	}

	/**
	 * Pops the last node off the end of the path.
	 *
	 * @return LDActiveRecordPathNode the last node in the path
	 * @throws CException if the path contains only one node (isEmpty === true). A path must always have a root node.
	 */
	public function pop()
	{
		if(count($this) <= 1)
		{
			throw new CException('The root of a path cannot be popped.');
		}
		$node = array_pop($this->_path);
		$this->getLast()->unlinkSink();
		return $node;
	}

	/**
	 * Shifts the first node off the beginning of the path.
	 * 
	 * @return LDActiveRecordPathNode the first node in the path
	 * @throws CException if the path contains only one node (isEmpty === true). A path must always have a root node.
	 */
	public function shift()
	{
		if(count($this) <= 1)
		{
			throw new CException('The root of a path cannot be shifted.');
		}
		$node = array_shift($this->_path);
		$this->getFirst()->unlinkSource();
		return $node;
	}

	/**
	 * Appends a node to the end of the path.
	 * 
	 * @param mixed $node
	 *        	the node to be appeded to the end of the path
	 */
	public function push($path)
	{
		foreach((array)$path as $segment)
		{
			list($relationName, $relationParams) = self::extractPathSegmentParts($segment);
			$this->_path[] = new LDActiveRecordPathNode($this->getLast(), $relationName, $relationParams);
		}
	}

	/**
	 * Prepends a node to the beginning of the path
	 * 
	 * @param mixed $node
	 *        	the node to be prepended to the beginning of the path
	 */
	public function unshift($path)
	{
		foreach((array)$path as $segment)
		{
			list($relationName, $relationParams) = self::extractPathSegmentParts($segment);
			array_unshift($this->_path, new LDActiveRecordPathNode($relationName, $this->getFirst(), $relationParams));
		}
	}

	/**
	 * Returns the number of nodes in the path.
	 * This method is required by Countable interface.
	 * 
	 * @return integer number of nodes in the path.
	 */
	public function count()
	{
		return count($this->_path);
	}

	/**
	 * Returns whether there is a path node at the specified offset.
	 * This method is required by the interface ArrayAccess.
	 * 
	 * @param integer $offset
	 *        	the offset to check on
	 * @return boolean
	 */
	public function offsetExists($offset)
	{
		return array_key_exists($offset, $this->_path);
	}

	/**
	 * Returns the path node at the specified offset.
	 * This method is required by the interface ArrayAccess.
	 * 
	 * @param integer $offset
	 *        	the offset to retrieve path node.
	 * @return mixed the path node at the offset
	 * @throws CException if the offset is invalid
	 */
	public function offsetGet($offset)
	{
		if($this->offsetExists($offset))
		{
			return $this->_path[$offset];
		}
		throw new CException('Path index "' . $offset . '" is out of bound.');
	}

	/**
	 * NOT IMPLEMENTED! This method is required by the interface ArrayAccess.
	 * To modify the path use methods: {@link pop()}, {@link shift()}, {@link push}, {@link unshift()}
	 * 
	 * @param integer $offset        	
	 * @param mixed $value        	
	 */
	public function offsetSet($offset, $value)
	{
		throw new CException('Not implemented');
	}

	/**
	 * NOT IMPLEMENTED! This method is required by the interface ArrayAccess.
	 * To modify the path use methods: {@link pop()}, {@link shift()}, {@link push}, {@link unshift()}
	 * 
	 * @param integer $offset        	
	 */
	public function offsetUnset($offset)
	{
		throw new CException('Not implemented');
	}
	
	public function getName($reverseFlow = false)
	{
		return implode(self::NAME_GLUE, array_merge(array($this->getFirst()->getModelName()), $this->getRelationNodeNames($reverseFlow)));
	}
	
	public function __toString()
	{
		return $this->getName();
	}

	/**
	 * Utility method that checks the path segment is valid and then extracts the path segment parts (relation parameters and relation name)
	 *
	 * @param mixed $segment        	
	 * @throws CException
	 * @return array The relation parameters followed by the relation name
	 */
	public static function extractPathSegmentParts($segment)
	{
		// Extract the relation and parameters from the path segment
		if(empty($segment)) // Invalid path segment
		{
			throw new CException('Path segment was empty. A path segment must lead somewhere...');
		}

		if(is_array($segment)) // relation with attributes ("relation name" => attributes)
		{
			$params = reset($segment);
			$relation = key($segment);
			if(!is_array($params)) // Params was not an array so assume relation name only (no params)
			{
				$relation = (string)$params;
				$params = array();
			}
		}
		else // relation name only (no params)
		{
			$relation = $segment;
			$params = array();
		}

		return array(
			$relation instanceof CActiveRelation ? $relation->name : (string)$relation,
			$params
		);
	}

}

class LDActiveRecordPathIterator implements Iterator
{

	/**
	 *
	 * @var LDActiveRecordPath the path to be iterated through
	 */
	private $_path;

	/**
	 *
	 * @var integer index of the current item
	 */
	private $_i;

	/**
	 * Constructor.
	 *
	 * @param LDActiveRecordPath $path
	 *        	the path to be iterated through
	 */
	public function __construct(&$path)
	{
		$this->_path = &$path;
		$this->_i = 0;
	}

	/**
	 * Rewinds internal path pointer.
	 * This method is required by the interface Iterator.
	 */
	public function rewind()
	{
		$this->_i = 0;
	}

	/**
	 * Returns the key of the current path node.
	 * This method is required by the interface Iterator.
	 *
	 * @return integer the key of the current path node
	 */
	public function key()
	{
		return $this->_i;
	}

	/**
	 * Returns the current path node.
	 * This method is required by the interface Iterator.
	 *
	 * @return LDActiveRecordPathNode the current path node
	 */
	public function current()
	{
		return $this->_path[$this->_i];
	}

	/**
	 * Moves the internal pointer to the next path node.
	 * This method is required by the interface Iterator.
	 */
	public function next()
	{
		$this->_i++;
	}

	/**
	 * Returns whether there is a path node at the current position.
	 * This method is required by the interface Iterator.
	 *
	 * @return boolean
	 */
	public function valid()
	{
		return $this->_i < count($this->_path);
	}

}
